package io.dflabs.demo.presenter.callbacks;

import java.util.ArrayList;

import io.dflabs.demo.model.pojos.Post;

public interface PostsCallback {

    void onLoadingPosts();

    void onErrorLoadingPosts();

    void onSuccessLoadingPosts(ArrayList<Post> posts);
}
