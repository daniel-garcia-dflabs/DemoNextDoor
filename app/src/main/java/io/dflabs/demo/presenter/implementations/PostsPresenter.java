package io.dflabs.demo.presenter.implementations;

import android.support.annotation.NonNull;

import java.util.ArrayList;

import io.dflabs.demo.background.ws.WebServices;
import io.dflabs.demo.model.pojos.Post;
import io.dflabs.demo.presenter.callbacks.PostsCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostsPresenter {

    private PostsCallback postsCallback;
    private Call<ArrayList<Post>> mCall;

    public PostsPresenter(PostsCallback postsCallback) {
        this.postsCallback = postsCallback;
    }

    public void onCreate() {

    }

    public void onDestroy() {
        if (mCall != null && mCall.isExecuted()) {
            mCall.cancel();
        }
    }

    public void fetchPosts() {
        this.postsCallback.onLoadingPosts();
        mCall = WebServices.api().fetchPosts();
        mCall.enqueue(new Callback<ArrayList<Post>>() {
            @Override
            public void onResponse(@NonNull Call<ArrayList<Post>> call,
                                   @NonNull Response<ArrayList<Post>> response) {
                if (response.isSuccessful()) {
                    postsCallback.onSuccessLoadingPosts(response.body());
                } else {
                    postsCallback.onErrorLoadingPosts();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Post>> call, Throwable t) {
                t.printStackTrace();
                postsCallback.onErrorLoadingPosts();
            }
        });
    }
}
