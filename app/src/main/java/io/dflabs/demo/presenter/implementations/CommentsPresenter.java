package io.dflabs.demo.presenter.implementations;

import java.util.ArrayList;

import io.dflabs.demo.background.ws.WebServices;
import io.dflabs.demo.model.pojos.Comment;
import io.dflabs.demo.presenter.callbacks.CommentsCallback;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentsPresenter {
    private CommentsCallback commentsCallback;
    private Call<ArrayList<Comment>> mCall;

    public CommentsPresenter(CommentsCallback commentsCallback) {
        this.commentsCallback = commentsCallback;
    }

    public void onCreate() {

    }

    public void fetchComments(String postId) {
        this.commentsCallback.onLoadingComments();
        mCall = WebServices.api().fetchComments(postId);
        mCall.enqueue(new Callback<ArrayList<Comment>>() {
            @Override
            public void onResponse(Call<ArrayList<Comment>> call, Response<ArrayList<Comment>> response) {
                if (response.isSuccessful()) {
                    commentsCallback.onSuccessLoadedComments(response.body());
                } else {
                    commentsCallback.onErrorLoadedComments();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Comment>> call, Throwable t) {
                t.printStackTrace();
                commentsCallback.onErrorLoadedComments();
            }
        });
    }

    public void onDestroy() {

    }
}
