package io.dflabs.demo.presenter.callbacks;

import java.util.ArrayList;

import io.dflabs.demo.model.pojos.Comment;

public interface CommentsCallback {
    void onSuccessLoadedComments(ArrayList<Comment> comments);

    void onErrorLoadedComments();

    void onLoadingComments();
}
