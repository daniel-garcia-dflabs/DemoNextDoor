package io.dflabs.demo.background.ws;

import io.dflabs.demo.background.ws.definitions.ApiDefinitionV1;
import io.dflabs.demo.library.Utils;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class WebServices {
    public static ApiDefinitionV1 api() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(Utils.getUnsafeOkHttpClient())
                .build();
        return retrofit.create(ApiDefinitionV1.class);
    }
}
