package io.dflabs.demo.background.ws.definitions;

import java.util.ArrayList;

import io.dflabs.demo.model.pojos.Comment;
import io.dflabs.demo.model.pojos.Post;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;


public interface ApiDefinitionV1 {

    @GET("/posts")
    Call<ArrayList<Post>> fetchPosts();

    @GET("/posts/{postId}/comments")
    Call<ArrayList<Comment>> fetchComments(@Path("postId") String postId);
}
