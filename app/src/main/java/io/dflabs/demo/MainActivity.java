package io.dflabs.demo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import io.dflabs.demo.model.pojos.Post;
import io.dflabs.demo.presenter.callbacks.PostsCallback;
import io.dflabs.demo.presenter.implementations.PostsPresenter;
import io.dflabs.demo.view.activities.DetailPostActivity;
import io.dflabs.demo.view.adapters.PostsAdapter;

public class MainActivity extends AppCompatActivity implements PostsCallback {

    private RecyclerView mRecyclerView;
    private PostsAdapter mPostsAdapter;
    private PostsPresenter mPresenter;
    private View.OnClickListener mOnPostClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Post post = (Post) v.getTag();
            DetailPostActivity.launch(MainActivity.this, post);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle(R.string.title_main_activity);

        mRecyclerView = findViewById(R.id.act_main_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mPostsAdapter = new PostsAdapter(mOnPostClickListener));

        mPresenter = new PostsPresenter(this);
        mPresenter.onCreate();

        mPresenter.fetchPosts();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
    }

    @Override
    public void onLoadingPosts() {
        Toast.makeText(this, R.string.dialog_loading_posts, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onErrorLoadingPosts() {
        Toast.makeText(this, R.string.dialog_error_loading_posts, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onSuccessLoadingPosts(ArrayList<Post> posts) {
        mPostsAdapter.update(posts);
    }
}
