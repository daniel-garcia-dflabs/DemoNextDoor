package io.dflabs.demo.model.pojos;

import java.io.Serializable;

public class Comment implements Serializable {

    public String id;
    public String name;
    public String email;
    public String body;
}
