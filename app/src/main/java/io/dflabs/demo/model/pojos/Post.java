package io.dflabs.demo.model.pojos;

import java.io.Serializable;

public class Post implements Serializable {
    public String userId;
    public String id;
    public String title;
    public String body;

    @Override
    public String toString() {
        return this.title;
    }
}
