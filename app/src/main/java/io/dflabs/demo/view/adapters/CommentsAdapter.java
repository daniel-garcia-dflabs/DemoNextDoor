package io.dflabs.demo.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import io.dflabs.demo.R;
import io.dflabs.demo.model.pojos.Comment;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.CommentsViewHolder> {

    private ArrayList<Comment> mItems = new ArrayList<>();

    @NonNull
    @Override
    public CommentsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_comment, parent, false);
        return new CommentsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentsViewHolder holder, int position) {
        Comment comment = mItems.get(position);
        holder.mEmailTextView.setText(comment.email);
        holder.mNameTextView.setText(comment.name);
        holder.mTextView.setText(comment.body);
        holder.itemView.setTag(comment);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void update(ArrayList<Comment> comments) {
        this.mItems = comments;
        notifyDataSetChanged();
    }

    class CommentsViewHolder extends RecyclerView.ViewHolder {

        private final TextView mTextView;
        private final TextView mNameTextView;
        private final TextView mEmailTextView;

        CommentsViewHolder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.item_comment_text);
            mNameTextView = itemView.findViewById(R.id.item_comment_name);
            mEmailTextView = itemView.findViewById(R.id.item_comment_email);
        }
    }
}
