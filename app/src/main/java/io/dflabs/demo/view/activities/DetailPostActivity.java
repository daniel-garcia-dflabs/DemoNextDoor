package io.dflabs.demo.view.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.dflabs.demo.R;
import io.dflabs.demo.library.Keys;
import io.dflabs.demo.model.pojos.Comment;
import io.dflabs.demo.model.pojos.Post;
import io.dflabs.demo.presenter.callbacks.CommentsCallback;
import io.dflabs.demo.presenter.implementations.CommentsPresenter;
import io.dflabs.demo.view.adapters.CommentsAdapter;

public class DetailPostActivity extends AppCompatActivity implements CommentsCallback {

    private CommentsAdapter mCommentsAdapter;
    private CommentsPresenter mCommentsPresenter;

    public static void launch(Context context, Post post) {
        Intent intent = new Intent(context, DetailPostActivity.class);
        intent.putExtra(Keys.EXTRA_POST, post);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_post);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        setTitle(R.string.title_detail_post_activity);

        Post post = (Post) getIntent().getSerializableExtra(Keys.EXTRA_POST);

        TextView mTitleTextView = findViewById(R.id.act_detail_title);
        TextView mDetailTextView = findViewById(R.id.act_detail_body);

        mTitleTextView.setText(post.title);
        mDetailTextView.setText(post.body);

        RecyclerView mCommentsRecyclerView = findViewById(R.id.act_detail_comments);
        mCommentsRecyclerView.setAdapter(mCommentsAdapter = new CommentsAdapter());
        mCommentsRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mCommentsPresenter = new CommentsPresenter(this);

        mCommentsPresenter.onCreate();
        mCommentsPresenter.fetchComments(post.id);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSuccessLoadedComments(ArrayList<Comment> comments) {
        mCommentsAdapter.update(comments);
    }

    @Override
    public void onErrorLoadedComments() {
        Toast.makeText(this, R.string.dialog_error_loading_comments, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoadingComments() {
        Toast.makeText(this, R.string.dialog_loading_comments, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mCommentsPresenter.onDestroy();
    }
}
