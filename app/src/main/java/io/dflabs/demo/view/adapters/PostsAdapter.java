package io.dflabs.demo.view.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import io.dflabs.demo.R;
import io.dflabs.demo.model.pojos.Post;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.PostsViewHolder> {

    private ArrayList<Post> mItems = new ArrayList<>();
    private View.OnClickListener onPostClickListener;

    public PostsAdapter(View.OnClickListener onPostClickListener) {
        this.onPostClickListener = onPostClickListener;
    }

    @NonNull
    @Override
    public PostsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_post, parent, false);
        return new PostsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PostsViewHolder holder, int position) {
        Post post = mItems.get(position);
        holder.mTitleTextView.setText(post.title);
        holder.mBodyTextView.setText(post.body);
        holder.itemView.setTag(post);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void update(ArrayList<Post> posts) {
        this.mItems = posts;
        notifyDataSetChanged();
    }

    class PostsViewHolder extends RecyclerView.ViewHolder {

        TextView mTitleTextView;
        TextView mBodyTextView;

        PostsViewHolder(View itemView) {
            super(itemView);
            mTitleTextView = itemView.findViewById(R.id.item_post_title);
            mBodyTextView = itemView.findViewById(R.id.item_post_body);
            itemView.setOnClickListener(onPostClickListener);
        }
    }
}
